#include <stdio.h>
#include <stdlib.h>
#include <glew.h>
#include <glfw3.h>
#include "shader.h"
#include "shape.h"
#include "glm/ext.hpp"

GLFWwindow* window;
GLuint VertexArrayID;

Shape** shapes;
const int SHAPE_SIZE = 6;
Hierarchy* hierarchies;
const int HIERARCHY_SIZE = 2;
const float LIGHT_INTENSITY = 8;
const float SPECULAR_MULTIPLIER = 0.5;
const float SHINYNESS = 32;
const Vertex AMBIENCE_MULTIPLIER(0.1, 0.1, 0.1);
int WINDOW_WIDTH = 800, WINDOW_HEIGHT = 800;
Mesh teapot("Object/Groudon.obj");
Vertex light_source = Vertex(0.2, 0.8, 2);
Vertex light_color = Vertex(1, 1, 1);
Vertex eye = Vertex(0, 1, 2);

//Requirements
void InitializeGLEW()
{
	if (glewInit() != GLEW_OK)
	{
		printf("Failed to initialize GLEW\n");
		glfwTerminate();
		return;
	}
}

void MouseMoveEvent(GLFWwindow* window, double x, double y)
{
	double mod_x = (float)(x - (WINDOW_WIDTH / 2)) / (float)(WINDOW_WIDTH / 2);
	double mod_y = (float)(WINDOW_HEIGHT - y - (WINDOW_HEIGHT / 2)) / (float)(WINDOW_HEIGHT / 2);
	//printf("X : %f, Y : %f\n", mod_x, mod_y);
}

void MouseClickEvent(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT)
	{
		double x, y;
		glfwGetCursorPos(window, &x, &y);

		double mod_x = (float)(x - (WINDOW_WIDTH / 2)) / (float)(WINDOW_WIDTH / 2);
		double mod_y = (float)(WINDOW_HEIGHT - y - (WINDOW_HEIGHT / 2)) / (float)(WINDOW_HEIGHT / 2);
		if (action == GLFW_PRESS)
		{
			//printf("LEFT CLICK ON : (%lf, %lf)\n", mod_x, mod_y);
		}
		else if (action == GLFW_RELEASE)
		{
			//printf("LEFT RELEASE ON : (%lf, %lf)\n", mod_x, mod_y);
		}
	}
}

void KeyboardPressEvent(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_A && action == GLFW_REPEAT)
	{
		for (int i = 0; i < SHAPE_SIZE; i++)
		{
			shapes[i]->Translate(Vertex(-0.01, 0, 0));
			teapot.Translate(Vertex(0.01, 0, 0));
		}
	}
	else if (key == GLFW_KEY_D && action == GLFW_REPEAT)
	{
		for (int i = 0; i < SHAPE_SIZE; i++)
		{
			shapes[i]->Translate(Vertex(0.01, 0, 0));
			teapot.Translate(Vertex(-0.01, 0, 0));
		}
	}
	else if (key == GLFW_KEY_W && action == GLFW_REPEAT)
	{
		for (int i = 0; i < SHAPE_SIZE; i++)
		{
			shapes[i]->Translate(Vertex(0, 0.01, 0));
			teapot.Translate(Vertex(0, -0.01, 0));
		}
	}
	else if (key == GLFW_KEY_S && action == GLFW_REPEAT)
	{
		for (int i = 0; i < SHAPE_SIZE; i++)
		{
			shapes[i]->Translate(Vertex(0, -0.01, 0));
			teapot.Translate(Vertex(0, 0.01, 0));
		}
	}
	else if (key == GLFW_KEY_Z && action == GLFW_REPEAT)
	{
		for (int i = 0; i < SHAPE_SIZE; i++)
		{
			shapes[i]->Translate(Vertex(0, 0, 0.01));
			teapot.Translate(Vertex(0, 0, 0.01));
		}
	}
	else if (key == GLFW_KEY_X && action == GLFW_REPEAT)
	{
		for (int i = 0; i < SHAPE_SIZE; i++)
		{
			shapes[i]->Translate(Vertex(0, 0, -0.01));
			teapot.Translate(Vertex(0, 0, -0.01));
		}
	}
	else if (key == GLFW_KEY_Q && action == GLFW_REPEAT)
	{
		for (int i = 0; i < SHAPE_SIZE; i++)
		{
			shapes[i]->Rotate(shapes[i]->GetPosition(), shapes[i]->GetEuler(1), 1);
		}
		teapot.Rotate(eye, Vertex(0, 1, 0), -1);
	}
	else if (key == GLFW_KEY_E && action == GLFW_REPEAT)
	{
		for (int i = 0; i < SHAPE_SIZE; i++)
		{
			shapes[i]->Rotate(shapes[i]->GetPosition(), shapes[i]->GetEuler(1), -1);
		}
		teapot.Rotate(eye, Vertex(0, 1, 0), 1);
	}
	teapot.CalculateColors(light_source, light_color, AMBIENCE_MULTIPLIER, eye, SPECULAR_MULTIPLIER, SHINYNESS, LIGHT_INTENSITY);
}

void ScreenResizeEvent(GLFWwindow* window, int width, int height)
{
	WINDOW_WIDTH = width;
	WINDOW_HEIGHT = height;
	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
}

void InitializeGLFW()
{
	if (!glfwInit())
	{
		printf("Failed to initialize GLFW\n");
		glfwTerminate();
		return;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "OpenGL", NULL, NULL);
	if (window == NULL)
	{
		printf("Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		return;
	}
	glfwMakeContextCurrent(window);
	glfwSetCursorPosCallback(window, MouseMoveEvent);
	glfwSetMouseButtonCallback(window, MouseClickEvent);
	glfwSetKeyCallback(window, KeyboardPressEvent);
	glfwSetFramebufferSizeCallback(window, ScreenResizeEvent);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	glEnable(GL_DEPTH_TEST);
}

//Shapes
void InitializeShapes()
{
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	shapes = new Shape * [SHAPE_SIZE];
	Vertex control_pts[9] =
	{
		Vertex(0, -0.8),
		Vertex(-0.1, -0.6),
		Vertex(-0.2, -0.7),
		Vertex(-0.25, -0.5),
		Vertex(-0.35, -0.4),
		Vertex(-0.4, 0.1),
		Vertex(-0.5, 0.15),
		Vertex(-0.65, 0.3),
		Vertex(-0.75, 0.5)
	};
	shapes[0] = new Vase(control_pts, 9, 0, -0.5, 0.5, 30, 1.0, 30); //Tornado 1
	shapes[1] = new Vase(control_pts, 9, 0, -0.5, -1.5, 30, 1.0, 30); //Tornado 2
	shapes[2] = new Prism(0.15f, 0.15f, 0.10f, -0.2, -0.5f, 0.5f); //Roof 1
	shapes[3] = new Box(0.15f, 0.15f, 0.15f, -0.2, -0.625f, 0.5f); //Body 1
	shapes[4] = new Prism(0.15f, 0.15f, 0.10f, 0.3, -0.4f, -1.5f); //Roof 2
	shapes[5] = new Box(0.15f, 0.15f, 0.15f, 0.3, -0.525f, -1.5f); //Body 2

	char vertexShader[SHAPE_SIZE][100] = 
	{ 
		"shaders/tornado/vertex.shader",
		"shaders/tornado/vertex.shader",
		"shaders/house/roof/vertex.shader",
		"shaders/house/body/vertex.shader",
		"shaders/house/roof/vertex.shader",
		"shaders/house/body/vertex.shader"
	};
	char fragmentShader[SHAPE_SIZE][100] = 
	{ 
		"shaders/tornado/fragment.shader",
		"shaders/tornado/fragment.shader",
		"shaders/house/roof/fragment.shader",
		"shaders/house/body/fragment.shader",
		"shaders/house/roof/fragment.shader",
		"shaders/house/body/fragment.shader"
	};
	char fragmentOutlineShader[SHAPE_SIZE][100] = 
	{ 
		"shaders/tornado/fragment_outline.shader",
		"shaders/tornado/fragment_outline.shader",
		"shaders/house/roof/fragment_outline.shader",
		"shaders/house/body/fragment_outline.shader",
		"shaders/house/roof/fragment_outline.shader",
		"shaders/house/body/fragment_outline.shader"
	};

	for (int i = 0; i < SHAPE_SIZE; i++)
	{
		shapes[i]->InitializeBuffer();
		shapes[i]->InitializeShader(vertexShader[i], fragmentShader[i]);
		shapes[i]->InitializeOutlineShader(vertexShader[i], fragmentOutlineShader[i]);
	}

	hierarchies = new Hierarchy[HIERARCHY_SIZE];
	hierarchies[0].SetParent(shapes[0]);
	hierarchies[0].AddChild(new Hierarchy(shapes[2]));
	hierarchies[0].GetChild(0)->AddChild(new Hierarchy(shapes[3]));

	hierarchies[1].SetParent(shapes[1]);
	hierarchies[1].AddChild(new Hierarchy(shapes[4]));
	hierarchies[1].GetChild(0)->AddChild(new Hierarchy(shapes[5]));

	teapot.InitializeBuffer();
	teapot.InitializeShader("shaders/teapot/vertex.shader", "shaders/teapot/fragment.shader");
	teapot.InitializeOutlineShader("shaders/teapot/vertex.shader", "shaders/teapot/fragment_outline.shader");
	teapot.CalculateColors(light_source, light_color, AMBIENCE_MULTIPLIER, eye, SPECULAR_MULTIPLIER, SHINYNESS, LIGHT_INTENSITY);

	Vertex mik(5, 0, 0);
	Vertex* matrix = new Vertex[3];
	GetRotationMatrix(Vertex(0, 0, 1), 90 * DEG_TO_RAD, matrix);
	for (int i = 0; i < 3; i++)
	{
		matrix[i].Print();
	}
	GetRotationResult(Vertex(5, 15, 0), matrix, mik).Print();
}

//Rendering Stuff
void Render()
{
	int house_move_direction[HIERARCHY_SIZE] = { 1 };
	float house_move_boundary[HIERARCHY_SIZE][2] = 
	{
		{-0.8f, -0.2f}
	};
	float house_move_speed[HIERARCHY_SIZE] = { 0.01 };
	do
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Groudon
		teapot.DrawPolygon();
		//teapot.DrawPolyline();
		//teapot.RandomizeColors();

		//Tornado Start
		for (int i = 0; i < SHAPE_SIZE; i++)
		{
			shapes[i]->DrawPolygon();
			shapes[i]->DrawPolyline();
		}
		
		hierarchies[1].Rotate(hierarchies[1].GetParent()->GetPosition(), hierarchies[1].GetParent()->GetEuler(1), 2);
		hierarchies[1].Rotate(Vertex(0, -0.5, -0.5), Vertex(0, 1, 0), 0.1);

		hierarchies[1].GetChild(0)->Rotate(
			hierarchies[1].GetChild(0)->GetParent()->GetPosition(),
			hierarchies[1].GetChild(0)->GetParent()->GetEuler(0),
			2
		);

		hierarchies[1].GetChild(0)->Rotate(
			hierarchies[1].GetParent()->GetPosition(),
			hierarchies[1].GetParent()->GetEuler(0),
			2
		);

		hierarchies[0].Rotate(hierarchies[0].GetParent()->GetPosition(), hierarchies[0].GetParent()->GetEuler(1), 2);
		hierarchies[0].Rotate(Vertex(0, -0.5, -0.5), Vertex(0, 1, 0), 0.1);

		hierarchies[0].GetChild(0)->Rotate(
			hierarchies[0].GetChild(0)->GetParent()->GetPosition(),
			hierarchies[0].GetChild(0)->GetParent()->GetEuler(1), 
			2
		);

		hierarchies[0].GetChild(0)->Rotate(
			hierarchies[0].GetParent()->GetPosition(),
			hierarchies[0].GetParent()->GetEuler(0),
			2
		);
		//Tornado End

		glfwSwapBuffers(window);
		glfwPollEvents();
	} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0);
	glDisableVertexAttribArray(0);
}

void CleanUp()
{
	delete shapes;
	glDeleteVertexArrays(1, &VertexArrayID);
	glfwTerminate();
}

void main()
{
	srand(time(NULL));
	InitializeGLFW();
	InitializeGLEW();
	InitializeShapes();
	Render();
	CleanUp();
}

//Updated files
//http://f.petra.ac.id/opengl3_materi
//http://f.petra.ac.id/opengl3_shape

//Old files
//http://f.petra.ac.id/opengl3_shader_h
//http://f.petra.ac.id/opengl3_project
//http://f.petra.ac.id/opengl3_shader_pack