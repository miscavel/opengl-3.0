# OpenGL 3.0

A remake of a previous project using GLUT https://gitlab.com/Miscavel/opengl in GLFW and GLEW.

Has better class structure and slightly improved performance.

![Typhoon](video/typhoon.mp4)

![Groudon .obj with lighting and camera](video/groudon.mp4)